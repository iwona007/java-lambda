package Iwona.pl;

import Iwona.pl.ex1.RandomNumbers;
import Iwona.pl.ex2.Email;
import Iwona.pl.ex2.EmailManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> numbers = new ArrayList<>();

        RandomNumbers randomNumbers = new RandomNumbers();
//        randomNumbers.generateNumbers(); // for
        randomNumbers.random(numbers, 10, () -> random.nextInt(20));
        randomNumbers.printNumbers(numbers, element -> System.out.print(element + " "));
        System.out.println();
        randomNumbers.filter(numbers, element -> element % 2 == 0);

//        Ex2
        List<Email> list = EmailManager.createEmailList();
        List<Email> filter = EmailManager.filter(list, email -> email.isSent());
        System.out.println(filter);
        List<Email> filter1 = EmailManager.filter(list, email -> email.getSender().equals("bart@example.com") ||
                email.getRecipient().equals("bart@example.com"));
        System.out.println(filter1);

    }
}
