package Iwona.pl.ex1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class RandomNumbers {
    Random random = new Random();
    List<Integer> numbers = new ArrayList<>();

    //1 spsob
    public void generateNumbers() {
//       T get();
        Supplier<Integer> supplier = () -> random.nextInt(20);
        List<Integer> numbers2 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            numbers2.add(supplier.get());
        }
        for (Integer number : numbers2) {
            System.out.print(number + " ");
        }
    }

    //    drugi sposób
    public <T> void random(List<T> list, int number, Supplier<T> supplier) {
        for (int i = 0; i < number; i++) {
            list.add(supplier.get());
        }
    }

    //    Consumer to print value  accept(t);
    public <T> void printNumbers(List<T> list, Consumer<T> consumer) {
        for (T t : list) {
            consumer.accept(t);
        }
    }

    public static <T> void filter(List<T> list, Predicate<T> predicate) {
        Iterator<T> iterator = list.iterator();
        while (iterator.hasNext()) {
            T temporary = iterator.next();
            if (predicate.test(temporary)) {
                iterator.remove();
            }
        }
    }
}
