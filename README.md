# java-lambda
ex1
Napisz aplikację, w której z wykorzystaniem klasy Random i wyrażenia lambda wygenerujesz
 10 losowych liczb (interfejs Supplier). Wyświetl listę (wszystkie liczby obok siebie, po spacji) 
 na ekranie również wykorzystując wyrażenia lambda (interfejs Consumer).

Następnie napisz metodę, która usunie z listy wszystkie liczby podzielne przez 2 
(użyj iteratora i własnej metody wykorzystującej interfejs Predicate).

Ponownie wyświetl wynik na ekranie.
Zdefiniuj kilka metod generycznych, 
które pozwolą wykonać czynności opisane w zadaniu.

ex2:
Kod programu, w którym zdefiniowana jest klasa Email, 
która reprezentuje wiadomość elektroniczną, oraz lista obiektów tego typu. 
W klasie EmailManager zdefiniowanych jest także kilka metod.

Metody, które posiadają w nazwie słowo filter pozwalają przefiltrować listę 
i zwracają w wyniku listę z obiektami spełniającymi określony warunek. Problem polega 
na tym, że kod tych metod jest bardzo powtarzalny. Wykorzystując odpowiednie interfejsy 
funkcyjne i wyrażenia lambda, przerób kod programu, aby było w nim jak najmniej powtarzalnego kodu.

Zdefiniuj jedną metodę filtrującą, która oprócz listy przyjmuje drugi parametr, 
który będzie predykatem służącym do filtrowania.
